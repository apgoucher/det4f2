///bin/cat /dev/null; echo 'Compiling...'
///bin/cat /dev/null; g++ -Wall -Wextra --std=c++17 -O3 lowerbound.cpp -o lowerbound -pthread
///bin/cat /dev/null; echo '...compiled.'
///bin/cat /dev/null; ./lowerbound "$@"; status=$?
///bin/cat /dev/null; exit $status

#include <stdint.h>
#include <iostream>
#include <iomanip>
#include <algorithm>
#include <vector>
#include <atomic>
#include <thread>
#include <sstream>

// transpose 4x4 bitmatrix:
uint16_t transpose_4x4(uint16_t a) {

    uint16_t d = (a ^ (a >> 3)) & 0x0a0a;
    uint16_t b = a ^ d ^ (d << 3);
    uint16_t e = (b ^ (b >> 6)) & 0x00cc;
    uint16_t c = b ^ e ^ (e << 6);

    return c;
}

// obtain the set of 225 rank-1 matrices:
void get_rank_1_matrices(uint16_t *mats) {

    int k = 0;

    for (int i = 1; i < 16; i++) {
        for (int j = 1; j < 16; j++) {
            mats[k++] = i * transpose_4x4(j);
        }
    }

    // enforce lexicographical order:
    std::sort(mats, mats + 225);
}


// naive determination of rank by counting elements in kernel:
int determine_rank(const uint16_t *a, int n) {

    uint64_t kcount = 0;

    // iterate over span of a in Gray code order:
    uint16_t image = 0;
    for (uint64_t i = 1; i < (1ull << n); i++) {
        image ^= a[__builtin_ctzll(i)];
        kcount += ((bool) (image == 0));
    }

    // now, kcount contains the number of nonzero vectors (one less
    // than a power of two) that are in the kernel; compute the
    // dimension by counting the ones in the binary representation:
    int nullity = __builtin_popcountll(kcount);

    // use rank-nullity theorem:
    return n - nullity;
}

/**
 * The main backtracking code.
 *
 * The current depth I is given as a template parameter, rather than
 * an ordinary parameter, to avoid true recursion (which incurs function
 * call overheads). This way, the compiler will inline this code to
 * produce N nested loops.
 */
template<int N, int I = 0, typename Fn>
void backtracking_search(int si, uint16_t *proj_stack, uint16_t *mats_stack, const uint16_t *proj, const uint16_t *mats, Fn lambda) {

    for (int x = si; x < 225; x++) {
        mats_stack[I] = mats[x];
        proj_stack[I] = proj[x];

        // determine whether it is impossible for the current initial
        // segment {A_0, ..., A_I} to be the initial segment of a valid
        // solution.
        int proj_rank = determine_rank(proj_stack, I+1);
        if (proj_rank > N - 6) { continue; }
        int mats_rank = determine_rank(mats_stack, I+1);
        if (mats_rank - proj_rank + (N-1-I) < 6) { continue; }

        if constexpr (I == N-1) {
            // {A_0, ..., A_{N-1}} span the antisymmetric subspace.
            lambda(mats_stack);
        } else {
            // recurse deeper into the search tree:
            backtracking_search<N, I+1>(x, proj_stack, mats_stack, proj, mats, lambda);
        }
    }
}

// project onto 10-dimensional subspace:
uint16_t project(uint16_t x) {
    return (x & 0xf731) ^ (transpose_4x4(x) & 0x7310);
}

// Implement the check described in Lemma 6 of Appendix B.
bool is_eliminated(const uint16_t* res, int n, const uint16_t* mats) {

    for (int i = 0; i < 225; i++) {
        uint16_t x = mats[i];
        int p = 0;
        for (int j = 0; j < n; j++) {
            p += (__builtin_popcount(res[j] & x) & 1);
        }
        if (p == 1) { return true; }
    }

    return false;
}


template<int N>
void run_subtask(uint32_t task_id, const uint16_t *mats, const uint16_t *proj) {

    // unpack subtask
    uint32_t i0 = (task_id >> 24) & 255;
    uint32_t i1 = (task_id >> 16) & 255;
    uint32_t i2 = (task_id >>  8) & 255;
    uint32_t i3 = task_id & 255;

    uint16_t mats_stack[N] = {0};
    uint16_t proj_stack[N] = {0};

    mats_stack[0] = mats[i0]; proj_stack[0] = project(mats_stack[0]);
    mats_stack[1] = mats[i1]; proj_stack[1] = project(mats_stack[1]);
    mats_stack[2] = mats[i2]; proj_stack[2] = project(mats_stack[2]);
    mats_stack[3] = mats[i3]; proj_stack[3] = project(mats_stack[3]);

    backtracking_search<N, 4>(i3, proj_stack, mats_stack, proj, mats, [&](const uint16_t *res) {

        if (is_eliminated(res, N, mats)) { return; }

        // This solution spans the antisymmetric subspace and passes the test
        // in Lemma 5. We now print the solution by writing it into a string:
        std::ostringstream ss;
        ss << "solution: [";
        for (int i = 0; i < N; i++) {
            ss << "0x" << std::setfill('0') << std::setw(4) << std::hex << res[i] << ", ";
        }
        ss << "]";

        // Output the solution to standard output:
        std::cout << ss.str() << std::endl;
    });
}


template<int N>
void run_worker(std::atomic<uint32_t> *ctr, uint32_t n_tasks, const uint32_t *tasks, const uint16_t *mats, const uint16_t *proj) {

    for (;;) {
        // dequeue subtask:
        uint32_t idx = (uint32_t) ((*ctr)++);
        if (idx >= n_tasks) { break; }

        if ((idx % 1000) == 0) {
            std::cerr << "Running subtask " << idx << std::endl;
        }

        // randomise to give more uniform progress indication:
        uint32_t task_id = tasks[(((uint64_t) idx) * 0x40000full) % ((uint64_t) n_tasks)];

        run_subtask<N>(task_id, mats, proj);
    }
}


template<int N>
void exhaust_parallel(int parallelism) {

    // obtain all rank-1 matrices:
    uint16_t mats[225];
    get_rank_1_matrices(mats);

    // compute projections:
    uint16_t proj[225];
    for (int i = 0; i < 225; i++) {
        // XOR upper-triangle with strict upper triangle of transpose:
        proj[i] = project(mats[i]);
    }

    std::vector<uint32_t> tasks;

    // We split the big depth-N search tree into a bunch of smaller
    // depth-(N-4) subtrees. Each of these subtrees forms a task
    // which we enqueue to be picked up by the next available worker
    // thread.
    for (uint32_t i0 = 0; i0 < 2; i0++) {
        for (uint32_t i1 = i0; i1 < 225; i1++) {
            for (uint32_t i2 = i1; i2 < 225; i2++) {
                for (uint32_t i3 = i2; i3 < 225; i3++) {
                    uint32_t task = (i0 << 24) ^ (i1 << 16) ^ (i2 << 8) ^ i3;
                    tasks.push_back(task);
                }
            }
        }
    }

    std::cerr << tasks.size() << " subtasks created for " << parallelism << " workers." << std::endl;

    std::atomic<uint32_t> ctr{0};

    std::vector<std::thread> workers;

    for (int i = 0; i < parallelism; i++) {
        workers.emplace_back(run_worker<N>, &ctr, tasks.size(), &(tasks[0]), mats, proj);
    }

    for (auto&& w : workers) {
        w.join();
    }
}



int main(int argc, char* argv[]) {

    if (argc < 2) {
        std::cerr << "Usage: ./lowerbound.cpp number_of_threads > solutions.txt" << std::endl;
        return 1;
    }

    int n_threads = std::stoll(argv[1]);

    if (n_threads < 1) {
        std::cerr << "Error: number of threads must be positive" << std::endl;
        return 1;
    }

    // We search the N=11 space in order to establish a lower bound of 12.
    exhaust_parallel<11>(n_threads);

    return 0;
}
