This code performs the computer search described in Appendix B of the
[Houston-Goucher-Johnston paper][1]. This prints all sets of eleven rank-1
matrices $`A_1, A_2, \dots, A_{11}`$ which contain the 6-dimensional
antisymmetric subspace in their linear span and moreover pass the test
described in Lemma 6.

We assume that $`0x0002 \geq A_1 \leq A_2 \leq \cdots \leq A_{11}`$
without loss of generality, where binary matrices are identified with
16-bit unsigned integers.

To compile the program, you need a C++17 compiler:

    g++ -Wall -Wextra --std=c++17 -O3 lowerbound.cpp -o lowerbound -pthread

Then run with the following command (warning: takes 357 core-hours):

    ./lowerbound NUMBER_OF_THREADS > solutions.txt

The resulting solution file should contain exactly 44 candidate sets, all
of which are equivalent up to symmetry. (If we did not impose the constraint
that $`A_1 \leq 0x0002`$, then it would take much longer and produce 840
sets instead of 44.)

[1]: https://arxiv.org/abs/2301.06586
